# README

## How to run the program
1. docker-compose build
2. docker-compose up -d
3. docker exec -it rails-web bin/rails db:create
4. docker exec -it rails-web bin/rails db:migrate

## Curl Payload

### Check patient endpoint
```
curl --request GET http://localhost:3000/patients/IC000A2
```
### Example Response Check patient endpoint
```
{"id":1,"id_number":"IC000A2","name":"Patient A4","gender":"F","date_of_birth":"19940231","phone_mobile":null,"created_at":"2021-03-14T15:58:18.370Z","updated_at":"2021-03-14T15:58:18.370Z"}
```

### Get patient endpoint
```
curl --request GET http://localhost:3000/patients
```

### Example Response Get patient endpoint
```
[{"id":1,"id_number":"IC000A2","name":"Patient A4","gender":"F","date_of_birth":"19940231","phone_mobile":null,"created_at":"2021-03-14T15:24:55.436Z","updated_at":"2021-03-14T15:24:55.436Z"},{"id":2,"id_number":"IC000A3","name":"Patient A5","gender":"M","date_of_birth":"19940231","phone_mobile":"+6500000000","created_at":"2021-03-14T15:25:03.323Z","updated_at":"2021-03-14T15:25:03.323Z"}]
```

### POST endpoint Service Payload Format 1 
```
curl --header "Content-Type:application/json" --request POST --data '{"id_number":"IC000A2","patient_name":"Patient A4","gender":"F","date_of_birth":"19940231", "date_of_test":"20210227134300","lab_number":"QT196-21-124","clinic_code":"QT196", "lab_studies":[{"code":"2085-9","name":"HDL Cholesterol","value":"cancel","unit":"mg/dL","ref_range":"> 59","finding":"A","result_state":"F"}]}' http://localhost:3000/patients -v
```

### Example Response Service Payload Format 1 
```
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 3000 (#0)
> POST /patients HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.64.1
> Accept: */*
> Content-Type:application/json
> Content-Length: 318
> 
* upload completely sent off: 318 out of 318 bytes
< HTTP/1.1 201 Created
< X-Frame-Options: SAMEORIGIN
< X-XSS-Protection: 1; mode=block
< X-Content-Type-Options: nosniff
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Referrer-Policy: strict-origin-when-cross-origin
< Content-Type: application/json; charset=utf-8
< Vary: Accept
< Cache-Control: no-store, must-revalidate, private, max-age=0
< X-Request-Id: 51d1dec4-9f51-480c-8074-b0c0ef834841
< X-Runtime: 0.075386
< X-MiniProfiler-Original-Cache-Control: max-age=0, private, must-revalidate
< X-MiniProfiler-Ids: 547llofuq16ivs5cvj77,69vrp82bl5974x1rzns4,z0sq5e8fpusorylrad5
< Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly; SameSite=Lax
< Transfer-Encoding: chunked
< 
* Connection #0 to host localhost left intact
{"id":1,"date_of_test":"20210227134300","lab_number":"QT196-21-124","clinic_code":"QT196","code":"2085-9","name":"HDL Cholesterol","value":"cancel","unit":"mg/dL","ref_range":"\u003e 59","finding":"A","result_state":"F","patient_id":1,"created_at":"2021-03-14T15:24:55.469Z","updated_at":"2021-03-14T15:24:55.469Z"}* Closing connection 0
```

### POST endpoint Service Payload Format 2
```
curl --header "Content-Type:application/json" --request POST --data '{"patient_data": { "id_number":"IC000A3",  "first_name":"Patient",  "last_name":"A5",  "phone_mobile":"+6500000000",  "gender":"M",  "date_of_birth":"19940231"}, "date_of_test":"20210227134300","lab_number":"QT196-21-124","clinic_code":"QT196", "lab_studies":[{"code":"2085-9","name":"HDL Cholesterol","value":"cancel","unit":"mg/dL","ref_range":"> 59","finding":"A","result_state":"F"}]}' http://localhost:3000/patients -v
```

### Example Response Service Payload Format 2
```
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 3000 (#0)
> POST /patients HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.64.1
> Accept: */*
> Content-Type:application/json
> Content-Length: 388
> 
* upload completely sent off: 388 out of 388 bytes
< HTTP/1.1 201 Created
< X-Frame-Options: SAMEORIGIN
< X-XSS-Protection: 1; mode=block
< X-Content-Type-Options: nosniff
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Referrer-Policy: strict-origin-when-cross-origin
< Content-Type: application/json; charset=utf-8
< Vary: Accept
< Cache-Control: no-store, must-revalidate, private, max-age=0
< X-Request-Id: b818f424-fbf9-477b-8670-7f29e090fbb3
< X-Runtime: 0.025425
< X-MiniProfiler-Original-Cache-Control: max-age=0, private, must-revalidate
< X-MiniProfiler-Ids: dgwueihx9fv7w6vrjstt,69vrp82bl5974x1rzns4,z0sq5e8fpusorylrad5,547llofuq16ivs5cvj77
< Set-Cookie: __profilin=p%3Dt; path=/; HttpOnly; SameSite=Lax
< Transfer-Encoding: chunked
< 
* Connection #0 to host localhost left intact
{"id":2,"date_of_test":"20210227134300","lab_number":"QT196-21-124","clinic_code":"QT196","code":"2085-9","name":"HDL Cholesterol","value":"cancel","unit":"mg/dL","ref_range":"\u003e 59","finding":"A","result_state":"F","patient_id":2,"created_at":"2021-03-14T15:25:03.328Z","updated_at":"2021-03-14T15:25:03.328Z"}* Closing connection 0
```

class CreatePatients < ActiveRecord::Migration[6.1]
  def change
    create_table :patients do |t|
      t.string :id_number
      t.string :name
      t.string :gender
      t.string :date_of_birth
      t.string :phone_mobile

      t.timestamps
    end
  end
end

class Patient < ApplicationRecord
    has_many :patient_labs

    validates :id_number, presence: true, uniqueness: true
    validates :name, presence: true
    validates :gender, presence: true
    validates :date_of_birth, presence: true
    validates :phone_mobile, uniqueness: true
end

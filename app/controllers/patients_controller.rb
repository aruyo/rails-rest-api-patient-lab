class PatientsController < ApplicationController
    def index
        @patiens = Patient.all
        render json: @patiens
    end

    def create
        
        if params[:patient_data] != nil
            patient_data = params[:patient_data]
            patient = Patient.find_by(id_number: patient_data[:id_number])
        else
            patient = Patient.find_by(id_number: params[:id_number])
        end

        if patient == nil
            patient = Patient.new

            if params[:patient_data] != nil
                patient_data = params[:patient_data]
                patient.name = patient_data[:first_name] + ' ' + patient_data[:last_name]
                patient.id_number = patient_data[:id_number]
                patient.gender = patient_data[:gender]
                patient.date_of_birth = patient_data[:date_of_birth]
                patient.phone_mobile = patient_data[:phone_mobile]
            else
                patient.name = params[:patient_name]
                patient.id_number = params[:id_number]
                patient.gender = params[:gender]
                patient.date_of_birth = params[:date_of_birth]
            end
        
            if patient.save
            #   render json: patient, status: :created
            else
              render json: patient.errors, status: :unprocessable_entity
              return
            end
        end

        patient_lab = patient.patient_labs.new
        patient_lab.date_of_test = params[:date_of_test]
        patient_lab.lab_number = params[:lab_number]
        patient_lab.clinic_code = params[:clinic_code]

        lab_studies = params[:lab_studies][0]

        patient_lab.code = lab_studies[:code]
        patient_lab.name = lab_studies[:name]
        patient_lab.value = lab_studies[:value]
        patient_lab.unit = lab_studies[:unit]
        patient_lab.ref_range = lab_studies[:ref_range]
        patient_lab.finding = lab_studies[:finding]
        patient_lab.result_state = lab_studies[:result_state]

        if patient_lab.save
            render json: patient_lab, status: :created
        else
            render json: patient_lab.errors, status: :unprocessable_entity
        end
    end
    
    def show
        patient = Patient.find_by(id_number: params[:id])
        if patient == nil
            render json: {"status":"Not found","created_at":"null","updated_at":"null"}
        else
            render json: {"id_number": patient.id_number,"created_at": patient.created_at,"updated_at": patient.updated_at}
        end
    end
end
